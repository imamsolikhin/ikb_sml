docker rm $(docker stop $(docker ps -a -q --filter name="sml_be" --format="{{.ID}}"))
docker build -t "sml_be" .
docker run -i -d -p 8081:80 --name="sml_be_1" -v $PWD:/app "sml_be"
docker exec $(docker ps -a -q --filter name="sml_be_1" --format="{{.ID}}") /bin/bash service nginx start
docker run -i -d -p 8082:80 --name="sml_be_2" -v $PWD:/app "sml_be"
docker exec $(docker ps -a -q --filter name="sml_be_2" --format="{{.ID}}") /bin/bash service nginx start
