docker rm $(docker stop $(docker ps -a -q --filter name="sml_vdr" --format="{{.ID}}"))
docker build -t "sml_vdr" .
docker run -i -d -p 8081:80 --name="sml_vdr_1" -v $PWD:/app "sml_vdr"
docker exec $(docker ps -a -q --filter name="sml_vdr_1" --format="{{.ID}}") /bin/bash service nginx start
docker run -i -d -p 8082:80 --name="sml_vdr_2" -v $PWD:/app "sml_vdr"
docker exec $(docker ps -a -q --filter name="sml_vdr_2" --format="{{.ID}}") /bin/bash service nginx start
